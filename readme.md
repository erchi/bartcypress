**Phone Numbers Jest Test**  
Cypress testing of bart.sk

**Getting Started**  
1. npm install  

**Running the tests**  
2. npx cypress run  
or  
2. npx cypress open  
3. Click the button "Run all specs"  
**Built With**  
Cypress - provides a robust, complete framework for running automated tests but takes some of the freedom out of Selenium by confining the user to specific frameworks and languages.