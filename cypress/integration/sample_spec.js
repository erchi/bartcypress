describe('Visit the website and complete the form with error', function () {
    it('Visit website', function () {
        cy.visit("https://www.bart.sk/mam-zaujem-test")
    })
    it('Find name input and type name in', function () {
        cy.get('#name')
            .type('Erik Ruszinka')
            .should('have.value', 'Erik Ruszinka')
    })
    it('Find company name input and type in', function () {
        cy.get('#company-name')
            .type('Company Name')
            .should('have.value', 'Company Name')
    })
    it('Find company name input and type in', function () {
        cy.get('#email')
            .type('fake@email.com')
            .should('have.value', 'fake@email.com')
    })
    it('Find mobile phone input and type it in', function () {
        cy.get('#tel')
            .type('+421944923459')
            .should('have.value', '+421944923459')
    })
    it('Find the checkbox and click all the checkboxes', function () {
        cy.get('.form-group-check [type="checkbox"]')
            .not('[disabled]')
            .check().should('be.checked')
    })
    it('Find and click the send button without project description', function () {
        cy.get('#contact-submit')
            .click()
    })
    it('Find error text', function () {
        cy.get('.error-text').contains("Opíšte prosím svoj projekt")
    })
})

describe('Visit the website and complete the form properly(with slovak language)', function () {
    it('Visit website', function () {
        cy.visit("https://www.bart.sk/mam-zaujem-test")
    })
    it('Find name input and type name in', function () {
        cy.get('#name')
            .type('László Márthon')
            .should('have.value', 'László Márthon')
    })
    it('Find company name input and type in ', function () {
        cy.get('#company-name')
            .type('Márthon co.')
            .should('have.value', 'Márthon co.')
    })
    it('Find email input and type in wrong email', function () {
        cy.get('#email')
            .type('márthon@email.com')
            .should('have.value', 'márthon@email.com')
    })
    it('Find mobile phone input and type it in', function () {
        cy.get('#tel')
            .type('+421944923459')
            .should('have.value', '+421944923459')
    })
    it('Find the checkbox and click all the checkboxes', function () {
        cy.get('.form-group-check [type="checkbox"]')
            .not('[disabled]')
            .check().should('be.checked')
    })
    it('Type text to the message input', function () {
        cy.get('#message').type('Diakritické znaménko (z řeckého διακριτικός diakritikós = rozlišující) je znaménko ' +
            'v okolí písmene (nad ním, pod ním, vedle něj), které nějak pozměňuje význam písmene ' +
            '(nejčastěji označuje jeho odlišnou výslovnost).')
    })
    it('Find and click the send button without project description', function () {
        cy.get('#contact-submit')
            .click()
    })
    it('Find the modal overlay after successful sending', function () {
        cy.wait(2000)
            cy.get('.error-text').contains('Zadajte prosím valídny email')
    })
})

describe('Visit the website and complete the form properly(with slovak language)', function () {
    it('Visit website', function () {
        cy.visit("https://www.bart.sk/mam-zaujem-test")
    })
    it('Find name input and type name in', function () {
        cy.get('#name')
            .type('László Márthon')
            .should('have.value', 'László Márthon')
    })
    it('Find company name input and type in ', function () {
        cy.get('#company-name')
            .type('Márthon co.')
            .should('have.value', 'Márthon co.')
    })
    it('Find email input and type in proper email', function () {
        cy.get('#email')
            .type('marthon@email.com')
            .should('have.value', 'marthon@email.com')
    })
    it('Find mobile phone input and type it in', function () {
        cy.get('#tel')
            .type('+421944923459')
            .should('have.value', '+421944923459')
    })
    it('Find the checkbox and click all the checkboxes', function () {
        cy.get('.form-group-check [type="checkbox"]')
            .not('[disabled]')
            .check().should('be.checked')
    })
    it('Type text to the message input', function () {
        cy.get('#message').type('Diakritické znaménko (z řeckého διακριτικός diakritikós = rozlišující) je znaménko ' +
            'v okolí písmene (nad ním, pod ním, vedle něj), které nějak pozměňuje význam písmene ' +
            '(nejčastěji označuje jeho odlišnou výslovnost).')
    })
    it('Find and click the send button without project description', function () {
        cy.get('#contact-submit')
            .click()
    })
    it('Find the modal overlay after successful sending', function () {
        cy.wait(2000)
        cy.get('#modal-overlay-contact-us').contains("Ďakujem za vyplnenie žiadosti.")
    })
})

describe('Visit the website and complete the form properly', function () {
    it('Visit website', function () {
        cy.visit("https://www.bart.sk/mam-zaujem-test")
    })
    it('Find name input and type name in', function () {
        cy.get('#name')
            .type('Erik Ruszinka')
            .should('have.value', 'Erik Ruszinka')
    })
    it('Find company name input and type in', function () {
        cy.get('#company-name')
            .type('Company Name')
            .should('have.value', 'Company Name')
    })
    it('Find email input and type in', function () {
        cy.get('#email')
            .type('fake@email.com')
            .should('have.value', 'fake@email.com')
    })
    it('Find mobile phone input and type it in', function () {
        cy.get('#tel')
            .type('+421944923459')
            .should('have.value', '+421944923459')
    })
    it('Find the checkbox and click all the checkboxes', function () {
        cy.get('.form-group-check [type="checkbox"]')
            .not('[disabled]')
            .check().should('be.checked')
    })
    it('Type text to the message input', function () {
        cy.get('#message').type('Testing with Cypress.js')
    })
    it('Find and click the send button without project description', function () {
        cy.get('#contact-submit')
            .click()
    })
    it('Find the modal overlay after successful sending', function () {
        cy.wait(2000)
        cy.get('#modal-overlay-contact-us').should('be.visible')
    })
})

describe('Visit the website and complete the form properly with slovak(English Version)', function () {
    it('Visit website(English version)', function () {
        cy.visit("https://www.bart.sk/en/interested-in-test")
    })
    it('Find name input and type name in', function () {
        cy.get('#name')
            .type('László Márthon')
            .should('have.value', 'László Márthon')
    })
    it('Find company name input and type in ', function () {
        cy.get('#company-name')
            .type('Márthon co.')
            .should('have.value', 'Márthon co.')
    })
    it('Find email input and type in proper email', function () {
        cy.get('#email')
            .type('marthon@email.com')
            .should('have.value', 'marthon@email.com')
    })
    it('Find mobile phone input and type it in', function () {
        cy.get('#tel')
            .type('+421944923459')
            .should('have.value', '+421944923459')
    })
    it('Find the checkbox and click all the checkboxes', function () {
        cy.get('.form-group-check [type="checkbox"]')
            .not('[disabled]')
            .check().should('be.checked')
    })
    it('Type text to the message input', function () {
        cy.get('#message').type('Diakritické znaménko (z řeckého διακριτικός diakritikós = rozlišující) je znaménko ' +
            'v okolí písmene (nad ním, pod ním, vedle něj), které nějak pozměňuje význam písmene ' +
            '(nejčastěji označuje jeho odlišnou výslovnost).')
    })
    it('Find and click the send button without project description', function () {
        cy.get('#contact-submit')
            .click()
    })
    it('Find the modal overlay after successful sending', function () {
        cy.wait(2000)
        cy.get('#modal-overlay-contact-us').contains("Thank you for your request.")
    })
})

describe('Visit the website and complete the form properly with english(English Version)', function () {
    it('Visit website(English version)', function () {
        cy.visit("https://www.bart.sk/en/interested-in-test")
    })
    it('Find name input and type name in', function () {
        cy.get('#name')
            .type('John Doe')
            .should('have.value', 'John Doe')
    })
    it('Find company name input and type in ', function () {
        cy.get('#company-name')
            .type('John@company')
            .should('have.value', 'John@company')
    })
    it('Find email input and type in proper email', function () {
        cy.get('#email')
            .type('john@email.com')
            .should('have.value', 'john@email.com')
    })
    it('Find mobile phone input and type it in', function () {
        cy.get('#tel')
            .type('+1944923459')
            .should('have.value', '+1944923459')
    })
    it('Find the checkbox and click all the checkboxes', function () {
        cy.get('.form-group-check [type="checkbox"]')
            .not('[disabled]')
            .check().should('be.checked')
    })
    it('Type text to the message input', function () {
        cy.get('#message').type('Testing text for John Doe projects.')
    })
    it('Find and click the send button without project description', function () {
        cy.get('#contact-submit')
            .click()
    })
    it('Find the modal overlay after successful sending', function () {
        cy.wait(2000)
        cy.get('#modal-overlay-contact-us').contains("Thank you for your request.")
    })
})

